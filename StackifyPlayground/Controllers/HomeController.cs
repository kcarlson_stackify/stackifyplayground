﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StackifyLib;

namespace StackifyPlayground.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //log.Error("Index page intentional error.", new {Controller=Index, Action=Index});

            // Queue() lets you define the log level & send a message 
            StackifyLib.Logger.Queue("Info", "before things go #pearshaped");

            // .QueueException() is what makes exception available in Errors tool
            StackifyLib.Logger.QueueException("InvalidOperationException", new System.InvalidOperationException("Inside Home(). Oh, yeah!!!"));

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}