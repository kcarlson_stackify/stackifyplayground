﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StackifyPlayground.Startup))]
namespace StackifyPlayground
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
